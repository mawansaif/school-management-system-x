<!-- DataTales of Subjects -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="h2 font-weight-bold text-primary float-left">Teacher List</h6>
        <button class="float-right btn btn-primary print">Print</button>
    </div>
    <div class="card-body">
        <h2 class="h1 p-2 text-center font-weight-bold shadow m-4 text-primary bg-light">Working Teacher</h2>
        <div class="table-responsive mb-2">
            <table class="table table-bordered table-sm nowrap" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Teacher Id</th>
                        <th>UserName</th>
                        <th>Password</th>
                        <th>Name</th>
                        <th>Father Name</th>
                        <th>Phone #</th>
                        <th>Qualification</th>
                        <th>CNIC</th>
                        <th>Class Teacher</th>
                        <th>Email</th>
                        <th>D.o.B</th>
                        <th>Address</th>
                        <th>Religion</th>
                        <th>Ref Name</th>
                        <th>Ref NIC</th>
                        <th>Ref cell #</th>
                        <th>Designation</th>
                        <th>Gender</th>
                        <th>Status</th>
                        <th class="noprint" >Card</th>
                        <th class="noprint">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Teacher Id</th>
                        <th>UserName</th>
                        <th>Password</th>
                        <th>Name</th>
                        <th>Father Name</th>
                        <th>Phone #</th>
                        <th>Qualification</th>
                        <th>CNIC</th>
                        <th>Class Teacher</th>
                        <th>Email</th>
                        <th>D.o.B</th>
                        <th>Address</th>
                        <th>Religion</th>
                        <th>Ref Name</th>
                        <th>Ref NIC</th>
                        <th>Ref cell #</th>
                        <th>Designation</th>
                        <th>Gender</th>
                        <th>Status</th>
                        <th class="noprint">Card</th>
                        <th class="noprint">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($data['workingTeacher'] as $key => $teacher)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>
                            <div class="profile-Img">
                                <img src="{{ $teacher->teacher_profile_pic }}" width="100%" height="100%" alt="">
                            </div>
                        </td>
                        <td>{{ $teacher->teacher_id }}</td>
                        <td>{{ $teacher->user_name }}</td>
                        <td>{{ $teacher->password }}</td>
                        <td>{{ $teacher->teacher_name }}</td>
                        <td>{{ $teacher->teacher_father_name }}</td>
                        <td>{{ $teacher->teacher_phone}}</td>
                        <td>{{ $teacher->teacher_qualification}}</td>
                        <td>{{ $teacher->teacher_nic }}</td>
                        <td>
                            @if($teacher->is_class_teacher == 'yes')
                                <p class="bg-success text-center font-weight-bold text-white">Class Teacher</p>
                            @else
                                <p class="bg-dark text-center font-weight-bold text-white">Teacher</p>
                            @endif
                        </td>
                        <td>{{ $teacher->teacher_email }}</td>
                        <td>{{ $teacher->teacher_dob }}</td>
                        <td>{{ $teacher->teacher_address }}</td>
                        <td>{{ $teacher->teacher_religion }}</td>
                        <td>{{ $teacher->refrance_name }}</td>
                        <td>{{ $teacher->refrence_cnic }}</td>
                        <td>{{ $teacher->refrence_phone_no }}</td>
                        <td>{{ $teacher->teacher_designation }}</td>
                        <td>{{ $teacher->teacher_gender }}</td>
                        <td>
                            <p class="bg-success text-center font-weight-bold text-white">Working</p>
                        </td>
                        <td class="noprint">
                            <a class="btn btn-primary" href="print_teacher_card?teacher={{$teacher->id}}" target="_blank" >Print</a>
                        </td>
                        <td class="noprint">
                            <button class=" btn btn-success edit-btn" data-id="{{ $teacher->id }}" data-user="{{ $teacher->user_name }}" data-toggle="tooltip" data-placement="top" title="Edit Profile">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button class="btn btn-danger btn-delete"  data-id="{{ $teacher->id }}" data-toggle="tooltip" data-placement="top" title="Remove">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


        <h2 class="h1 p-2 text-center font-weight-bold shadow m-4 text-primary bg-light">Leaved Teacher</h2>
        <div class="table-responsive">
            <table class="table table-bordered table-sm nowrap" id="dataTable1" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Teacher Id</th>
                        <th>UserName</th>
                        <th>Password</th>
                        <th>Name</th>
                        <th>Father Name</th>
                        <th>Phone #</th>
                        <th>Qualification</th>
                        <th>CNIC</th>
                        <th>Class Teacher</th>
                        <th>Email</th>
                        <th>D.o.B</th>
                        <th>Address</th>
                        <th>Religion</th>
                        <th>Ref Name</th>
                        <th>Ref NIC</th>
                        <th>Ref cell #</th>
                        <th>Designation</th>
                        <th>Gender</th>
                        <th>Status</th>
                        <th class="noprint" >Card</th>
                        <th class="noprint">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Teacher Id</th>
                        <th>UserName</th>
                        <th>Password</th>
                        <th>Name</th>
                        <th>Father Name</th>
                        <th>Phone #</th>
                        <th>Qualification</th>
                        <th>CNIC</th>
                        <th>Class Teacher</th>
                        <th>Email</th>
                        <th>D.o.B</th>
                        <th>Address</th>
                        <th>Religion</th>
                        <th>Ref Name</th>
                        <th>Ref NIC</th>
                        <th>Ref cell #</th>
                        <th>Designation</th>
                        <th>Gender</th>
                        <th>Status</th>
                        <th class="noprint">Card</th>
                        <th class="noprint">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($data['leaveTeacher'] as $key => $teacher)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>
                            <div class="profile-Img">
                                <img src="{{ $teacher->teacher_profile_pic }}" width="100%" height="100%" alt="">
                            </div>
                        </td>
                        <td>{{ $teacher->teacher_id }}</td>
                        <td>{{ $teacher->user_name }}</td>
                        <td>{{ $teacher->password }}</td>
                        <td>{{ $teacher->teacher_name }}</td>
                        <td>{{ $teacher->teacher_father_name }}</td>
                        <td>{{ $teacher->teacher_phone}}</td>
                        <td>{{ $teacher->teacher_qualification}}</td>
                        <td>{{ $teacher->teacher_nic }}</td>
                        <td>{{ $teacher->is_class_teacher }}</td>
                        <td>{{ $teacher->teacher_email }}</td>
                        <td>{{ $teacher->teacher_dob }}</td>
                        <td>{{ $teacher->teacher_address }}</td>
                        <td>{{ $teacher->teacher_religion }}</td>
                        <td>{{ $teacher->refrance_name }}</td>
                        <td>{{ $teacher->refrence_cnic }}</td>
                        <td>{{ $teacher->refrence_phone_no }}</td>
                        <td>{{ $teacher->teacher_designation }}</td>
                        <td>{{ $teacher->teacher_gender }}</td>
                        <td>
                            <p class="bg-dark text-center font-weight-bold text-white">Leave</p>
                        </td>
                        <td class="noprint">
                            <a class="btn btn-primary" href="print_teacher_card?teacher={{$teacher->id}}" target="_blank" >Print</a>
                        </td>
                        <td class="noprint">
                            <button class=" btn btn-success edit-btn" data-id="{{ $teacher->id }}" data-user="{{ $teacher->user_name }}" data-toggle="tooltip" data-placement="top" title="Edit Profile">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button class="btn btn-danger btn-delete"  data-id="{{ $teacher->id }}" data-toggle="tooltip" data-placement="top" title="Remove">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(function () {
        $('[data-toggle="tooltip"]').tooltip()
        })
        $('#dataTable').DataTable();
        $('#dataTable1').DataTable();
    });
</script>
