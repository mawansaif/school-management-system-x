
@isset($data)
<span class="days-heading">Monday</span>
<div class="table-responsive">
    <table class="table table-sm table-bordered" id="dataTable1">
        <thead>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </thead>
        <tfoot>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </tfoot>
        <tbody>
            @foreach($data['monday'] as $key => $monday)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $monday->subject_title }}</td>
                <td>{{ $monday->class->class_title }} & {{ $monday->class->section_name }}</td>
                <td>{{ date("h:i A", strtotime($monday->lecture_start_time)) }} -- {{ date("h:i A", strtotime($monday->lecture_end_time)) }}</td>
                <td class="noprint">
                    <button class="btn btn-primary btn-edit noprint" data-id="{{ $monday->id }}">Edit</button>
                    <button class="btn btn-danger noprint btn-remove" data-id="{{ $monday->id }}">Delete</button>
                </td> 
            </tr>
            @endforeach
    </table>
</div>

<h1 class="days-heading">Tuesday</h1>
<div class="table-responsive">
    <table class="table table-sm table-bordered" id="dataTable2">
        <thead>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </thead>
        <tfoot>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </tfoot>
        <tbody>
            @foreach($data['tuesday'] as $key => $tuesday)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $tuesday->subject_title }}</td>
                <td>{{ $tuesday->class->class_title }} & {{ $tuesday->class->section_name }}</td>
                <td>{{ date("h:i A", strtotime($tuesday->lecture_start_time)) }} -- {{ date("h:i A", strtotime($tuesday->lecture_end_time)) }}</td>
                <td class="noprint">
                    <button class="btn btn-primary btn-edit noprint" data-id="{{ $tuesday->id }}">Edit</button>
                    <button class="btn btn-danger btn-remove noprint" data-id="{{ $tuesday->id }}">Delete</button>
                </td> 
            </tr>
            @endforeach
        </tbody>
    </table>
</div>


<h1 class="days-heading">Wednesday</h1>
<div class="table-responsive">
    <table class="table table-sm table-bordered" id="dataTable3">
        <thead>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </thead>
        <tfoot>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </tfoot>
        <tbody>
            @foreach($data['wednesday'] as $key => $wednesday)
            <tr>
                <td>{{ $key }}</td>
                <td>{{ $wednesday->subject_title }}</td>
                <td>{{ $wednesday->class->class_title }} & {{ $wednesday->class->section_name }} </td>
                <td> {{ date("h:i A", strtotime($wednesday->lecture_start_time)) }} -- {{ date("h:i A", strtotime($wednesday->lecture_end_time)) }} </td>
                <th class="noprint">
                    <button class="btn btn-primary btn-edit" data-id="{{ $wednesday->id }}">Edit</button>
                    <button class="btn btn-danger btn-remove" data-id="{{ $wednesday->id }}">Delete</button>
                </th>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>



<h1 class="days-heading">Thursday</h1>
<div class="table-responsive">
    <table class="table table-sm table-bordered" id="dataTable4">
        <thead>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </thead>
        <tfoot>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </tfoot>
        <tbody>
            @foreach($data['thursday'] as $key => $thursday)
            <tr>
                <td>{{ $key }}</td>
                <td>{{ $thursday->subject_title }}</td>
                <td>{{ $thursday->class->class_title }} & {{ $thursday->class->section_name }} </td>
                <td> {{ date("h:i A", strtotime($thursday->lecture_start_time)) }} -- {{ date("h:i A", strtotime($thursday->lecture_end_time)) }} </td>
                <th class="noprint">
                    <button class="btn btn-primary btn-edit" data-id="{{ $thursday->id }}">Edit</button>
                    <button class="btn btn-danger btn-remove" data-id="{{ $thursday->id }}">Delete</button>
                </th>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>



<h1 class="days-heading">Friday</h1>
<div class="table-responsive">
    <table class="table table-sm table-bordered" id="dataTable5">
        <thead>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </thead>
        <tfoot>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </tfoot>
        <tbody>
            @foreach($data['friday'] as $key => $friday)
            <tr>
                <td>{{ $key }}</td>
                <td>{{ $friday->subject_title }}</td>
                <td>{{ $friday->class->class_title }} & {{ $friday->class->section_name }} </td>
                <td> {{ date("h:i A", strtotime($friday->lecture_start_time)) }} -- {{ date("h:i A", strtotime($friday->lecture_end_time)) }} </td>
                <th class="noprint">
                    <button class="btn btn-primary btn-edit" data-id="{{ $friday->id }}">Edit</button>
                    <button class="btn btn-danger btn-remove" data-id="{{ $friday->id }}">Delete</button>
                </th>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<h1 class="days-heading">Saturday</h1>
<div class="table-responsive">
    <table class="table table-sm table-bordered" id="dataTable6">
        <thead>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </thead>
        <tfoot>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </tfoot>
        <tbody>
            @foreach($data['saturday'] as $key => $saturday)
            <tr>
                <td>{{ $key }}</td>
                <td>{{ $saturday->subject_title }}</td>
                <td>{{ $saturday->class->class_title }} & {{ $saturday->class->section_name }} </td>
                <td> {{ date("h:i A", strtotime($saturday->lecture_start_time)) }} -- {{ date("h:i A", strtotime($saturday->lecture_end_time)) }} </td>
                <th class="noprint">
                    <button class="btn btn-primary btn-edit" data-id="{{ $saturday->id }}">Edit</button>
                    <button class="btn btn-danger btn-remove" data-id="{{ $saturday->id }}">Delete</button>
                </th>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<h1 class="days-heading">Sunday</h1>
<div class="table-responsive">
    <table class="table table-sm table-bordered" id="dataTable7">
        <thead>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </thead>
        <tfoot>
            <th>#</th>
            <th>Subject Name</th>
            <th>Class & Section</th>
            <th>Time</th>
            <th class="noprint">Action</th>
        </tfoot>
        <tbody>
            @foreach($data['sunday'] as $key => $sunday)
            <tr>
                <td>{{ $key }}</td>
                <td>{{ $sunday->subject_title }}</td>
                <td>{{ $sunday->class->class_title }} & {{ $sunday->class->section_name }} </td>
                <td> {{ date("h:i A", strtotime($sunday->lecture_start_time)) }} -- {{ date("h:i A", strtotime($sunday->lecture_end_time)) }} </td>
                <th class="noprint">
                    <button class="btn btn-primary btn-edit" data-id="{{ $sunday->id }}">Edit</button>
                    <button class="btn btn-danger btn-remove" data-id="{{ $sunday->id }}">Delete</button>
                </th>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@else
    <div class="alert alert-warning p-2 font-weight-bold text-center">Please assign subjects to teacher.</div>
@endisset
<script>
    $(document).ready(function(){
        $("#dataTable1").dataTable();
        $("#dataTable2").dataTable();
        $("#dataTable3").dataTable();
        $("#dataTable4").dataTable();
        $("#dataTable5").dataTable();
        $("#dataTable6").dataTable();
        $("#dataTable7").dataTable();

    });
</script>