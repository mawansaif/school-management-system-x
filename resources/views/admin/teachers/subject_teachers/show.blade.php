<!-- DataTales of Subjects -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary float-left">Subject Teacher's List</h6>
        <button class="btn btn-success float-right btn-add">Add</button>
        <button class="btn btn-primary float-right mr-2 btn-print"><i class="fas fa-print"></i></button>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-sm nowrap" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Teacher Profile</th>
                        <th>Teacher Id</th>
                        <th>Teacher Name</th>
                        <th>Teacher Phone #</th>
                        <th>Teacher Email</th>
                        <th class="noprint">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Teacher Profile</th>
                        <th>Teacher Id</th>
                        <th>Teacher Name</th>
                        <th>Teacher Phone #</th>
                        <th>Teacher Email</th>
                        <th class="noprint">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @isset($Teachers)
                        @foreach($Teachers as $key => $subjectTeacher)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>
                                    <div class="profile-Img">
                                        <img src="{{ $subjectTeacher->teacher_profile_pic }}" alt="" width="100%" height="auto">
                                    </div>
                                </td>
                                <td>{{ $subjectTeacher->teacher_id }}</td>
                                <td>{{ $subjectTeacher->teacher_name}}</td>
                                <td><i class="fas fa-phone p-2"></i>{{ $subjectTeacher->teacher_phone }}</td>
                                <td>{{ $subjectTeacher->teacher_email }}</td>
                                <td class="noprint">
                                    <a class="btn btn-primary" href="subject_teacher_detail?teacher={{ $subjectTeacher->id }}" data-toggle="tooltip" data-placement="top" title="Detail">Detail</a>
                                    
                                </td>
                            </tr>
                        @endforeach
                    @endisset
                </tbody>
            </table>
        </div>
    </div>
</div>


 <script>
     $(document).ready(function() {
         $(function () {
         $('[data-toggle="tooltip"]').tooltip()
         })
         $('#dataTable').DataTable();
     });
 </script>