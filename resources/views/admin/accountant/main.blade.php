@extends('admin.layouts.app')

@section('content')
@if(Session::has('message'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  {{ Session::get('message') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="card shadow">
    <div class="card-header">
        <h3 class="h4 font-weight-bold text-primary m-0 float-left">Accountants List</h3>
        <button class="btn btn-success btn-add float-right">Add</button>
        <button class="btn btn-primary btn-print float-right mr-2">Print</button>
    </div>

    <div class="card-body">
        <div class="content"></div>
    </div>
</div>

<!-- add Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h5 font-weight-bold" id="exampleModalLongTitle">Add Acountant</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form  id="addAccountantForm">
                @csrf
                <div class="modal-body">
                    <div id="addMessage"></div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <img src="{{ asset('logo/school-logo.png') }}" alt="" id="accountant_img" class="img-thumbnail">
                            <input type="file" name="accountant_profile" id="accountant_profile" class="form-control form-control-file overflow-hidden mt-2">
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="user_name">User Name:</label>
                                    <input type="text" name="user_name" id="user_name" class="form-control" placeholder="user name">
                                </div>
                                <div class="col-sm-6">
                                    <label for="password">Password:</label>
                                    <input type="text" name="password" id="password" class="form-control" placeholder="password">
                                </div>
                            </div>
                            <hr>

                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="name">Full Name:</label>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Full Name">
                                </div>

                                <div class="col-sm-6">  
                                    <label for="father_name">Father Name:</label>
                                    <input type="text" name="father_name" id="father_name" class="form-control" placeholder="Father Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="cnic">CNIC #:</label>
                                    <input type="number" name="cnic_number" id="cnic_number" class="form-control" placeholder="CNIC">
                                </div>

                                <div class="col-sm-6">
                                    <label for="phone_number">Phone Number:</label>
                                    <input type="tel" name="phone_number" id="phone_number" class="form-control" placeholder="Phone Number">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="email">Email:</label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                </div>

                                <div class="col-sm-6">
                                    <label for="qualification">Qualification:</label>
                                    <input type="text" name="qualification" id="qualification" class="form-control" placeholder="qualification">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="gender">Gender:</label>
                                    <select name="gender" id="gender" class="form-control custom-select">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>

                                <div class="col-sm-6">
                                    <label for="dob">Date of birth:</label>
                                    <input type="date" name="dob" id="dob" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="address">Address</label>
                                    <input type="text" name="address" id="address" class="form-control" placeholder="Living Address">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary mr-auto" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- add modal -->

<!-- edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h5 font-weight-bold" id="exampleModalLongTitle">Update Acountant</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form  id="updateAccountantForm">
                @csrf
                <div class="modal-body">
                    <div id="updateMessage"></div>
                    <div class="form-group row">
                        <input type="hidden" name="accountant_id" id="accountant_id">
                        <input type="hidden" name="user_id" id="user_id">
                        <div class="col-sm-3">
                            <img src="{{ asset('logo/school-logo.png') }}" alt="" id="edit_accountant_img" class="img-thumbnail">
                            <input type="file" name="accountant_profile" id="edit_accountant_profile" class="form-control form-control-file overflow-hidden mt-2">
                            
                            <label for="working_status">Working Status:</label>
                            <select name="status" id="status" class="form-control">
                                <option id="working" value="1">Working</option>
                                <option id="leave" value="0">Leave</option>
                            </select>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="user_name">User Name:</label>
                                    <input type="text" name="user_name" id="edit_user_name" class="form-control" placeholder="user name">
                                </div>
                                <div class="col-sm-6">
                                    <label for="password">Password:</label>
                                    <input type="text" name="password" id="edit_password" class="form-control" placeholder="password">
                                </div>
                            </div>
                            <hr>

                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="name">Full Name:</label>
                                    <input type="text" name="name" id="edit_name" class="form-control" placeholder="Full Name">
                                </div>

                                <div class="col-sm-6">  
                                    <label for="father_name">Father Name:</label>
                                    <input type="text" name="father_name" id="edit_father_name" class="form-control" placeholder="Father Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="cnic">CNIC #:</label>
                                    <input type="number" name="cnic_number" id="edit_cnic_number" class="form-control" placeholder="CNIC">
                                </div>

                                <div class="col-sm-6">
                                    <label for="phone_number">Phone Number:</label>
                                    <input type="tel" name="phone_number" id="edit_phone_number" class="form-control" placeholder="Phone Number">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="email">Email:</label>
                                    <input type="email" name="email" id="edit_email" class="form-control" placeholder="Email">
                                </div>

                                <div class="col-sm-6">
                                    <label for="qualification">Qualification:</label>
                                    <input type="text" name="qualification" id="edit_qualification" class="form-control" placeholder="qualification">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="gender">Gender:</label>
                                    <select name="gender" id="edit_gender" class="form-control custom-select">
                                        <option id="male" value="male">Male</option>
                                        <option id="female" value="female">Female</option>
                                    </select>
                                </div>

                                <div class="col-sm-6">
                                    <label for="dob">Date of birth:</label>
                                    <input type="date" name="dob" id="edit_dob" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="address">Address</label>
                                    <input type="text" name="address" id="edit_address" class="form-control" placeholder="Living Address">
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary mr-auto" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- edit modal -->


<!-- delete Modal -->
<div class="modal fade bottom" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel"
  aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="delete_accountant" method="post">
        <div class="modal-body text-center">

          <i class="fa fa-exclamation-triangle fa-8x text-warning" aria-hidden="true"></i>

          <p class="h2 mt-2">Confirm to delete it?</p>
          @csrf
          <input type="hidden" name="accountant_id" id="remove_accountant">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary mr-auto" data-dismiss="modal">Close</button>
          <button class="btn btn-danger">Procced</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Delete Modal -->

@endsection

@section('script')
<script>
    $(document).ready(function () {
        $(".content").load('show_accountants_list');
        
        $(".btn-print").on('click', function(){
            $('.table').printThis();
        });

        $(".btn-add").on('click', function () {
            $("#addMessage").html('');
            $("#addMessage").removeClass();
            $("#addAccountantForm")[0].reset();
            $("#addModal").modal('show');
        });

        $("#accountant_profile").on('change',function(event){
            $("#accountant_img").attr('src', URL.createObjectURL(event.target.files[0]));
        });

        $("#addAccountantForm").on('submit',function(e){
            e.preventDefault();
            $.ajax({
                url: 'save_new_accountant',
                method: 'post',
                data: new FormData(this),
                processData:false,
                dataType:'JSON',
                contentType:false,
                cache:false,
                beforeSend:function()
                {
                    $("#addMessage").html('');
                    $("#addMessage").removeClass();
                },
                success:function(data)
                {
                    if(data.response == 0)
                    {
                        $.each(data.errors, function(i,v){
                            $("#addMessage").append('*' + ' ' + v + '<br>');

                        });
                        $("#addMessage").addClass(data.class);

                    }
                    else
                    {
                        $("#addMessage").append(data.message);
                        $("#addMessage").addClass(data.class);
                        $(".content").load('show_accountants_list');
                    }
                }
            });
        });
        $(document).on('click','.btn-edit' ,function(){
            $("#updateMessage").html('');
            $("#updateMessage").removeClass();
            $.ajax({
                url: 'edit_accountant_data',
                method: 'get',
                data: {
                    employ: $(this).attr('data-id'),
                },
                success:function(data)
                {
                    $("#accountant_id").val(data.accountant.id);
                    $("#user_id").val(data.user.id);
                    $("#edit_accountant_img").attr('src', data.accountant.accountant_profile_pic);
                    if(data.accountant.is_active == '1')
                    {
                        $("#working").attr('selected', true);
                    }
                    else
                    {
                        $("#leave").attr('selected', true);
                    }
                    $("#edit_user_name").val(data.accountant.user_name);
                    $("#edit_password").val(data.accountant.password);
                    $("#edit_name").val(data.accountant.name);
                    $("#edit_father_name").val(data.accountant.father_name);
                    $("#edit_cnic_number").val(data.accountant.cnic);
                    $("#edit_phone_number").val(data.accountant.phone_number);
                    $("#edit_email").val(data.accountant.email);
                    $("#edit_qualification").val(data.accountant.qualification);
                    $("#"+data.accountant.gender).attr('selected', true);
                    $("#edit_dob").val(data.accountant.dob);
                    $("#edit_address").val(data.accountant.address);
                    $("#editModal").modal('show');
                }
            });
        });

        $("#edit_accountant_profile").on('change',function(event){
            $("#edit_accountant_img").attr('src', URL.createObjectURL(event.target.files[0]));
        });

        $("#updateAccountantForm").on('submit',function(e){
            e.preventDefault();
            $.ajax({
                url: 'update_accountant_detail',
                method: 'post',
                data: new FormData(this),
                processData:false,
                dataType:'JSON',
                contentType:false,
                cache:false,
                beforeSend:function()
                {
                    $("#updateMessage").html('');
                    $("#updateMessage").removeClass();
                },
                success:function(data)
                {
                    if(data.response == 0)
                    {
                        $.each(data.errors, function(i,v){
                            $("#updateMessage").append('*' + ' ' + v + '<br>');

                        });
                        $("#updateMessage").addClass(data.class);

                    }
                    else
                    {
                        $("#updateMessage").append(data.message);
                        $("#updateMessage").addClass(data.class);
                        $(".content").load('show_accountants_list');
                    }
                }
            });
        });

        $(document).on('click', '.btn-remove', function(){
            $("#remove_accountant").val($(this).attr('data-id'));
            $("#warningModal").modal('show');
        });
    });
</script>
@endsection