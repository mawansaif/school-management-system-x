<div class="table-responsive">
    <table class="table table-bordered table-sm nowrap" id="dataTable">
        <thead>
            <tr>
                <th>#</th>
                <th>Profile</th>
                <th>User Name</th>
                <th>Password</th>
                <th>Name</th>
                <th>Father Name</th>
                <th>DoB</th>
                <th>CNIC #</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th>Qualification</th>
                <th>Gender</th>
                <th>Address</th>
                <th class="noprint">Card</th>
                <th class="noprint">Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Profile</th>
                <th>User Name</th>
                <th>Password</th>
                <th>Name</th>
                <th>Father Name</th>
                <th>DoB</th>
                <th>CNIC #</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th>Qualification</th>
                <th>Gender</th>
                <th>Address</th>
                <th class="noprint">Card</th>
                <th class="noprint">Action</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach($data as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>
                    <div class="profile-Img">
                        <img src="{{ asset($value->accountant_profile_pic) }}" alt="">
                    </div>
                </td>
                <td>{{ $value->user_name }}
                    @if($value->is_active == 1)
                        <span class="badge badge-success">working</span>
                    @else
                        <span class="badge badge-warning">leaved</span>
                    @endif
                </td>
                <td>{{ $value->password }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->father_name }}</td>
                <td>{{ $value->dob }}</td>
                <td>{{ $value->cnic }}</td>
                <td>{{ $value->phone_number }}</td>
                <td>{{ $value->email }}</td>
                <td>{{ $value->qualification }}</td>
                <td>{{ $value->gender }}</td>
                <td>{{ $value->address }}</td>
                <td class="noprint">
                    <a href="employ_card?employ={{ $value->id }}" target="_blank" class="btn btn-secondary">Card</a>
                </td>
                <td class="noprint">
                    <button class="btn btn-primary btn-edit" data-id="{{ $value->id }}">Edit</button>
                    <button class="btn btn-danger btn-remove" data-id="{{ $value->id }}">Delete</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function(){
        $("#dataTable").dataTable();
    });
</script>