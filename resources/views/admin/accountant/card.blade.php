<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$data->name}}|Print Card</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style type="text/css">
        @media print {
                @page { margin: 0; }
                body { margin: 1.6cm; }
        }
    </style>
</head>
<body>
    <div class="container">
        <button class="print btn btn-primary mt-4 noprint">Print</button>
        <div class="row">
            <div class="col-sm-6 m-auto">
                <h1>The Grammer School</h1>
            </div>
        </div>
        {{ $data }}
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(".print").on('click',function(){
                window.print();
            });
        });
    </script>
</body>
</html>