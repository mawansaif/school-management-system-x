@extends('accountant.layouts.app')

@section('content')
<div class="teacher-attendance-report ">
    <span id="heading">Fee Report</span>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <span class="heading">Name:</span>
                </div>
                <div class="col-md-6">
                    <span>{{ $data['student']->students->student_name }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <span class="heading">Roll No:</span>
                </div>
                <div class="col-md-6">
                    <span>{{ $data['student']->students->student_roll_no }}</span>
                </div>
            </div>
        
        </div>
        <div class="col-md-6">
            <div class="row">
            <div class="col-md-6">
                    <span class="heading">Total Fee:</span>
                </div>
                <div class="col-md-6">
                    <span>Rs.{{ $data['student']->student_fee }}</span>
                </div>
            </div>

                    
            <div class="row">
                <div class="col-md-6">
                    <span class="heading">Paid fee:</span>
                </div>
                <div class="col-md-6">
                    <span>Rs.{{ $data['student']->paid_fee }}</span>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <span class="heading">Remaining Fee:</span>
                </div>
                <div class="col-md-6">
                    <span>Rs.{{ $data['student']->remaining_fee }}</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card shadow">
    <div class="card-header">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="h1 text-primary font-weight-bold">Fee Vouchers List</h1>
            </div>
            <div class="col-sm-4">
                <div class="float-right">
                    
                    <button class="btn btn-primary btn-print">Print</button>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-sm" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Student</td>
                        <th>Roll No</th>
                        <th>Class & section</th>
                        <th>Invoice Number</th>
                        <th>Fee Amount</th>
                        <th>Fee's Month</th>
                        <th>Paid date</th>
                        <th class="noprint">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Student</td>
                        <th>Roll No</th>
                        <th>Class & section</th>
                        <th>Invoice Number</th>
                        <th>Fee Amount</th>
                        <th>Fee's Month</th>
                        <th>Paid date</th>
                        <th class="noprint">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                   @foreach($data['fee'] as $key => $fee)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $fee->student_fees->students->student_name }}</td>
                            <td>#{{ $fee->student_fees->students->student_roll_no }}</td>
                            <td>{{ $fee->student_fees->students->class->class_title }} | {{ $fee->student_fees->students->class->section_name }} </td>
                            <td>{{$fee->invoice_number}}</td>
                            <td>Rs.{{ $fee->fee_amount }}</td>
                            <td>{{ \Carbon\Carbon::parse($fee->fee_of_month)->toFormattedDateString()}} </td>
                            <td>{{ $fee->paid_date }}</td>
                            <td class="noprint">
                                @if($fee->is_paid == '0' and $fee->fee_of_month <= \Carbon\Carbon::create(null, null, 1)->toDateString() )
                                    <a href="student_fee_voucher_printout?voucher={{$fee->id}}" target="_blank"
                                        class="btn btn-primary print-fee-voucher">Print Slip</a>
                                    <button class="btn btn-success btn-edit" data-id="{{ $fee->id }}">Edit</button>
                                @else
                                @endif
                                
                            </td>
                        </tr>
                   @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer"></div>
</div>



<!-- edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Update fee detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="update_student_fee_detail">
                <div class="modal-body">
                    @csrf
                    <div id="updateMessage"></div>
                    <input type="hidden" name="student_fee_detail_id" id="student_fee_detail_id">
                    <div class="form-group row">

                      <div class="col-sm-6">
                          <label for="invoice_number">Invoice Number:</label>
                          <input type="text" name="invoice_number" id="invoice_number" class="form-control" disabled>
                      </div>

                        <div class="col-sm-6">
                            <label for="fee_amount">Fee Amount:</label>
                            <input type="number" name="fee_amount" id="fee_amount" class="form-control" disabled>
                        </div>

                    </div>

                    <div class="form-group row">
                        

                        <div class="col-sm-6">
                            <label for="fee's_month">Fee's Month:</label>
                            <input type="date" name="fee_of_month" id="fee_of_month" class="form-control">
                        </div>


                        <div class="col-sm-6">
                            <label for="paid_date">Paid Date:</label>
                            <input type="date" name="paid_date" id="paid_date" class="form-control">
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 m-auto">
                            <label for="is_paid">Paid</label>
                            <select name="is_paid" id="is_paid" class="form-control">
                                <option value="" selected hidden disabled>Select paid option</option>
                                <option value="1" id="yesPaid">Yes Paid</option>
                                <option value="0" id="noPaid">No't yet</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button  class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- edit modal -->
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $(".btn-edit").on('click',function(){
            $("#updateMessage").html('');
            $("#updateMessage").removeClass();
            $.ajax({
                url: 'edit_student_fee_by_accountant',
                method: 'get',
                data: {
                    student_fee_id: $(this).attr('data-id'),
                },
                success: function (data)
                {
                    $("#student_fee_detail_id").val(data.id);
                    $("#invoice_number").val(data.invoice_number);
                    $("#fee_amount").val(data.fee_amount);
                    $("#fee_of_month").val(data.fee_of_month);
                    $("#paid_date").val(data.paid_date);
                    if(data.is_paid == '0'){
                        $("#noPaid").attr('selected', true);
                    }
                    else{
                        $("#yesPaid").attr('selected', true);
                    }
                    $("#editModal").modal('show');
                }
            });
        });

        $("#update_student_fee_detail").on('submit', function(e){
            e.preventDefault();
            $.ajax({
                url: 'update_student_fee_detail_by_accountant',
                method: 'post',
                data: new FormData(this),
                processData:false,
                dataType: 'JSON',
                contentType:false,
                cache:false,
                beforeSend:function()
                {
                    $("#updateMessage").html('');
                    $("#updateMessage").removeClass();
                },
                success:function(data)
                {
                    if(data.response == 0)
                    {
                        $.each(data.errors, function(i,v){
                            $("#updateMessage").append('*' + ' ' + v + '<br>');
                        });
                        $("#updateMessage").addClass(data.class);
                    }
                    else
                    {
                        $("#updateMessage").append(data.message);
                        $("#updateMessage").addClass(data.class);
                        $("#content").load('student_fee_vouchers');
                    }
                }
            });
        });
    });
</script>
@endsection