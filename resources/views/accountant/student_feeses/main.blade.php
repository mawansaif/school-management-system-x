@extends('accountant.layouts.app')

@section('content')

<div class="card shadow">
    <div class="card-header">
        <h4 class="h4 text-primary font-weight-bold float-left">Students List</h4>
    </div>

    <div class="card-body">
        <div class="table-responsive ">
            <table class="table table-bordered mt-1 table-sm nowrap" id="dataTable2">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Roll Number</th>
                        <th>Name</th>
                        <th>Father Name</th>
                        <th>Class & Section</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th>Fee</th>
                        <th>Paid Fee</th>
                        <th>Remaining Fee</th>
                        <th>Invoice No</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @isset($data)
                    @foreach($data['students'] as $key => $student)
                        <tr class="text-center">
                            <td>{{ $key + 1 }}</td>
                            <td><div class="profile-Img">
                                <img src="{{ asset($student->students->student_profile_pic) }}" alt="" >
                            </div></td>
                            <td>{{ $student->students->student_roll_no }}</td>
                            <td>{{ $student->students->student_name }}</td>
                            <td>{{ $student->students->student_father_name }}</td>
                            <td>{{ $student->students->class->class_title }} & {{ $student->students->class->section_name }}</td>
                            <td>{{ $student->students->student_guardian_phone_no }}</td>
                            <td>{{ $student->students->student_email }}</td>
                            <td>Rs. {{ $student->student_fee }}</td>
                            <td>Rs. {{ $student->paid_fee }}</td>
                            <td>Rs. {{ $student->remaining_fee }}</td>
                            <td>{{ $student->invoice_number }}</td>
                            <td>
                                <a href="student_fee_vouchers?student={{ $student->id }}" class="btn btn-secondary">Vouchers</a>
                                <button class="btn btn-success">Edit</button>
                            </td>
                        </tr>
                    @endforeach
                    @endisset
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Roll Number</th>
                        <th>Name</th>
                        <th>Father Name</th>
                        <th>Class & Section</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th>Fee</th>
                        <th>Paid Fee</th>
                        <th>Remaining Fee</th>
                        <th>Invoice No</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('#dataTable2 tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" />' );
        });
        $('#dataTable2').DataTable({
            // "scrollX": true,
            "info": false,
            initComplete: function () 
            {
                // Apply the search
                this.api().columns().every( function () {
                    var that = this;
    
                    $( 'input', this.footer() ).on( 'keyup change clear', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
            }
        });
    });
</script>
@endsection