<!-- Sidebar -->
<ul class="navbar-nav sidebar sidebar-dark accordion" style="background-color: #303030 !important;"
  id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/home">
    <div class="sidebar-brand-icon">
      <img src="{{ asset('logo/accounting.png') }}" alt="{{Auth::user()->user_role}}">
    </div>
    <div class="sidebar-brand-text mx-3">{{Auth::user()->user_role}}</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="/home">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
  </li>


  <!-- Divider -->
  <hr class="sidebar-divider">


  <!-- check -->
  @if(Auth::user()->user_role == 'accountant')
  <!-- Heading -->
  <div class="sidebar-heading">
    Student Fee
  </div>

  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseStudentFee" aria-expanded="true"
      aria-controls="collapseTwo">
      <i class="fas fa-fw fa-eye"></i>
      <span>Show</span>
    </a>
    <div id="collapseStudentFee" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Student Fee:</h6>
        <a class="collapse-item" href="student_feeses">Show</a>
      </div>
    </div>
  </li>


  <!-- Heading -->
  <div class="sidebar-heading">
    Teacher Salaries
  </div>

  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseClasses" aria-expanded="true"
      aria-controls="collapseTwo">
      <i class="fas fa-fw fa-eye"></i>
      <span>Show</span>
    </a>
    <div id="collapseClasses" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Salaries:</h6>
        <a class="collapse-item" href="teachers_salaries">Teacher Salaries</a>
      </div>
    </div>
  </li>
  @else

  @endif


  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->