@extends('accountant.layouts.app')

@section('content')

@isset($data)
<div class="teacher-profile">
    <div class="row">
        <div class="col-sm-3 p-2  ">
            <div class="teacher-profile-img ">
                <img src="{{ asset($data['accountant']->accountant_profile_pic) }}" alt="" width="100%" height="100%">
            </div> 
        </div>
        <div class="col-sm-7 pl-1 teacher-profile-info ">
            <h1>{{ $data['accountant']->name }}</h1>
            <small>{{ $data['accountant']->user_name }}</small><br>
            <small>{{ $data['accountant']->cnic }}</small>
            <p>{{ $data['accountant']->qualification }}</p>
            <p>{{ $data['accountant']->gender }}</p>
            <address>{{ $data['accountant']->address }}</address>
        </div>

        <div class="col-sm-2">
            <button class="btn-print noprint">Print</button>
        </div>
    </div>
    <div class="col-sm-12 down-section p-0">
        <div class="row p-3">
            
            <div class="col-sm-12 teacher-profile-detail-right">
                <div class="row ">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="heading">Father Name:</span>
                            </div>
                            <div class="col-md-6">
                                <span>{{ $data['accountant']->father_name }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="heading">Date of Birth:</span>
                            </div>

                            <div class="col-md-6">
                                <span>{{ \Carbon\Carbon::parse($data['accountant']->dob)->toFormattedDateString() }}</span>
                            </div>
                        </div> 
                    </div>
                </div>

                <div class="row down-section">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="heading">Phone Number:</span>
                            </div>

                            <div class="col-md-6">
                                <span>{{ $data['accountant']->phone_number }}</span>
                            </div>
                        </div>  
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="heading">Email:</span>
                            </div>

                            <div class="col-md-6">
                                <p class="">{{ $data['accountant']->email }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="card shadow">
    <div class="card-header">
        <h5 class="h4 font-weight-bold text-primary float-left">Late Fee Student</h5>
        <button class="btn btn-primary btn-print-table float-right">Print</button>
    </div>

    <div class="card-body">
        <div class="card-responsive">
            <table class="table table-sm table-bordered table-hover" id="dataTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Name</th>
                        <th>Roll No</th>
                        <th>Class & Section</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th>Fee</th>
                        <th>Fee's Month</th>
                        <th class="noprint">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data['late_fee'] as $key => $lateFee)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>
                                <div class="profile-Img">
                                    <img src="{{ $lateFee->student_fees->students->student_profile_pic }}" alt="">
                                </div>
                            </td>
                            <td>{{ $lateFee->student_fees->students->student_name }}</td>
                            <td>{{ $lateFee->student_fees->students->student_roll_no }}</td>
                            <td>{{ $lateFee->student_fees->students->class->class_title }} & {{ $lateFee->student_fees->students->class->section_name }}</td>
                            <td>{{ $lateFee->student_fees->students->student_guardian_phone_no }}</td>
                            <td>{{ $lateFee->student_fees->students->student_email }}</td>
                            <td>Rs. {{ $lateFee->fee_amount }}</td>
                            <td>{{ \Carbon\Carbon::parse($lateFee->fee_of_month)->toFormattedDateString() }}</td>
                            <td class="noprint">
                                <a href="student_fee_vouchers?student={{ $lateFee->student_fees->id }}" class="btn btn-secondary">Detail</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Name</th>
                        <th>Roll No</th>
                        <th>Class & Section</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th>Fee</th>
                        <th>Fee's Month</th>
                        <th class="noprint">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@else
@endisset
@endsection

@section('script')

<script>
    $(document).ready(function () {

        $(".btn-print").on('click', function () {
            $(".teacher-profile").printThis();
        });

        $(".btn-print-table").on('click',function(){
            $(".table").printThis();
        });
       
    });
</script>
@endsection