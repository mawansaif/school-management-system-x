@extends('accountant.layouts.app')

@section('content')
<div class="card shadow">
    <div class="card-header">
        <h5 class="h4 font-weight-bold text-primary float-left">Teacher Salaries</h5>
        <!-- <button class="btn btn-success btn-add float-right mr-2">Add</button> -->
        <button class="btn btn-primary btn-print-table mr-2 float-right">Print</button>
    </div>

    <div class="card-body">
        <div class="card-responsive">
            <table class="table table-sm table-striped table-bordered nowrap table-hover" id="dataTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Name</th>
                        <th>Id</th>
                        <th>Salary</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Name</th>
                        <th>Id</th>
                        <th>Salary</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($data['salary'] as $key => $teacher)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>
                            <div class="profile-Img">
                                <img src="{{ asset($teacher->teachers->teacher_profile_pic)  }}" alt="">
                            </div>
                        </td>
                        <td>{{ $teacher->teachers->teacher_name }}</td>
                        <td>#{{ $teacher->teachers->teacher_id }}</td>
                        <td>Rs. {{ $teacher->salary }}</td>
                        <td>{{ $teacher->teachers->teacher_phone }}</td>
                        <td>{{ $teacher->teachers->teacher_email }}</td>
                        <td>
                            <a href="teacher_salaries_detail?teacher={{ $teacher->teachers->id }}"
                                class="btn btn-secondary">Detail</a>
                            <!-- <button class="btn btn-success btn-edit" data-id="{{ $teacher->id }}">Edit</button> -->
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- add Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h5" id="exampleModalLongTitle">Teacher Salary</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addTeacherSalary">
                <div class="modal-body">
                    <div id="addMessage"></div>
                    @csrf
                    <label for="teacher">Teacher:</label>
                    <select name="teacher" id="teacher" class="form-control custom-select">
                        <option value="" selected hidden disabled>Choose ..</option>
                        @foreach($data['teacher'] as $teacher)
                            <option value="{{ $teacher->id }}">{{ $teacher->teacher_name }} || {{ $teacher->teacher_id }}</option>
                        @endforeach
                    </select>
                    <label for="salary">Salary Package:</label>
                    <input type="number" name="salary" id="salary" class="form-control" placeholder="salary">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- add Modal -->
@endsection

@section('script')
<script>

    $(document).ready(function () {

        $(".btn-add").on('click', function () {
            $("#addMessage").html('');
            $("#addMessage").removeClass();
            $("#addModal").modal('show');
        });
        $(".btn-edit").on('click', function () {

            $("#updateMessage").html('');

        });

        $("#addTeacherSalary").on('submit', function(e){
            e.preventDefault();
            $.ajax({
                url: 'add_teacher_salary_by_accountant',
                method: 'post',
                data: new FormData(this),
                processData:false,
                dataType: 'JSON',
                contentType:false,
                cache:false,
                beforeSend:function()
                {
                    $("#addMessaage").html('');
                    $("#addMessage").removeClass();
                },
                success:function(data)
                {
                    if(data.response == 0)
                    {
                        $.each(data.errors, function(i,v){
                            $("#addMessage").append('*'+ ' '+ v + '<br>');
                        });
                        $("#addMessage").addClass(data.class);
                    }
                    else
                    {
                        $("#addMessage").html(data.message);
                        $("#addMessage").addClass(data.class);
                    }
                }
            });
        });
    });
</script>
@endsection