@extends('accountant.layouts.app')

@section('content')
<div class="card shadow">
    <div class="card-header">
        <span class="h3 text-primary text-capitalize float-left font-weight-bold">Teacher Salary Detail</span>
        <button class="btn btn-success btn-add float-right btn-add ml-2">Add</button>
        <button class="btn btn-primary float-right btn-print">Print</button>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-sm table-border nowrap table-striped table-hover table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Name</th>
                        <th>Teacher #</th>
                        <th>Phone Number</th>
                        <th>Salary</th>
                        <th>Advance Salary</th>
                        <th>Month's Salary</th>
                        <th>Remaining Salary</th>
                        <th class="noprint">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data['salarydetail'] as $key => $value)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>
                            <div class="profile-Img">
                                <img src="{{ $value->teachers->teacher_profile_pic }}" alt="">
                            </div>
                        </td>
                        <td>{{ $value->teachers->teacher_name }}</td>
                        <td>{{ $value->teachers->teacher_id }}</td>
                        <td>{{ $value->teachers->teacher_phone }}</td>
                        <td>Rs. {{ $value->teacherSalary->salary }}</td>
                        <td>Rs. {{ $value->advance_salary }}</td>
                        <td>{{ Carbon\Carbon::create($value->salary_of_month)->toFormattedDateString() }}</td>
                        <td>Rs. {{ $value->remaining_salary }}</td>
                        <td class="noprint">
                            <button class="btn btn-primary btn-edit" data-id="{{ $value->id }}">Edit</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Name</th>
                        <th>Teacher #</th>
                        <th>Phone Number</th>
                        <th>Salary</th>
                        <th>Advance Salary</th>
                        <th>Month's Salary</th>
                        <th>Remaining Salary</th>
                        <th class="noprint">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<!-- add Modal -->
<div class="modal fade" id="addSalaryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Salary Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addTeacherSalary">
                <div class="modal-body">
                    <div id="addMessage"></div>
                    @csrf
                    <input type="hidden" name="salary_id" value="{{ $data['salary']->id }}">
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label for="">Teacher:</label>
                            <select name="teacher" id="teacher" class="form-control custom-select">
                                <option value="{{ $data['teacher']->id }}" selected>{{ $data['teacher']->teacher_name }}
                                </option>
                            </select>
                        </div>

                        <div class="col-sm-6">
                            <label for="salary">Salary:</label>
                            <input type="number" name="salary_amount" value="{{ $data['salary']->salary }}"
                                id="salary_amount" class="form-control" required>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label for="">Advance Salary:</label>
                            <input type="number" name="advance_salary" id="advance_salary" class="form-control"
                                required>
                        </div>

                        <div class="col-sm-6">
                            <label for="salary_of_month">Salary of Month:</label>
                            <input type="date" name="salary_of_month" id="salary_of_month" class="form-control"
                                required>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label for="remaining_salary">Remaining Salary:</label>
                            <input type="number" name="remaining_salary" id="remaining_salary" class="form-control"
                                required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary mr-auto" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- add modal -->

<!-- edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Update Teacher Salary</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="updateTeacherSalary">
            <div class="modal-body">
                <div id="updateMessage"></div>
                @csrf
                <input type="hidden" name="salary_id" id="edit_salary_id">
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="teacher">Teacher:</label>
                        <select name="teacher" id="" class="form-control custom-select">
                            <option value="{{ $data['teacher']->id }}">{{ $data['teacher']->teacher_name }}</option>
                        </select>
                    </div>

                    <div class="col-sm-6">
                        <label for="salary">Salary</label>
                        <input type="number" name="salary_amount" id="edit_salary_amount" class="form-control" value="{{ $data['salary']->salary }}">
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="advance_salary">Advance Salary:</label>
                        <input type="number" name="advance_salary" id="edit_advance_salary" class="form-control">
                    </div>

                    <div class="col-sm-6">
                        <label for="salary_of_month">Salary of Month:</label>
                        <input type="date" name="salary_of_month" id="edit_salary_of_month" class="form-control">
                    </div>

                </div>

                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="remaining_salary">Remaining Salary:</label>
                        <input type="number" name="remaining_salary" id="edit_remaining_salary" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary mr-auto" data-dismiss="modal">Close</button>
                <button class="btn btn-primary">Save changes</button>
            </div>
        </form>
      </div>
    </div>
  </div>

@endsection()

@section('script')
<script>
    $(document).ready(function () {

        $(".btn-print").on('click', function () {
            $(".table").printThis();
        });

        $(".btn-add").on('click', function () {
            $("#addTeacherSalary")[0].reset();
            $("#addMessage").html('');
            $("#addMessage").removeClass();
            $("#addSalaryModal").modal('show');
        });


        // $(".content").load('show_teacher_Salaries_detail_at_accountant');

        $("#advance_salary").on('change', function () {
            $("#remaining_salary").val($("#salary_amount").val() - $(this).val());
        });

        $("#addTeacherSalary").on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: 'add_teacher_salary_by_accountant',
                method: 'post',
                data: new FormData(this),
                processData: false,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                beforeSend: function () {
                    $("#addMessage").html('');
                    $("#addMessage").removeClass();
                },
                success: function (data) {
                    if (data.response == 0) {
                        $.each(data.errors, function (i, v) {
                            $("#addMessage").append('*' + ' ' + v + '<br>');
                        });
                        $("#addMessage").addClass(data.class);
                    }
                    else {
                        $("#addMessage").html(data.message);
                        $("#addMessage").addClass(data.class);
                    }
                }
            });
        });

        $(".btn-edit").on('click', function () {
            $("#updateMessage").html('');
            $("#updateMessage").removeClass();
            $.ajax({
                url:  'edit_teacher_salary_at_account',
                method: 'get',
                data: {
                    salary_id: $(this).attr('data-id'),
                },
                beforeSend:function()
                {
                    $("#updateTeacherSalary")[0].reset();
                },
                success:function(data)
                {
                    $("#edit_salary_id").val(data.id);
                    $("#edit_advance_salary").val(data.advance_salary);
                    $("#edit_salary_of_month").val(data.salary_of_month);
                    $("#edit_remaining_salary").val(data.remaining_salary);
                    $("#editModal").modal('show');
                }
            });
            
        });

        $("#edit_advance_salary").on('change', function () {
            $("#edit_remaining_salary").val($("#edit_salary_amount").val() - $(this).val());
        });

        $("#updateTeacherSalary").on('submit', function(e){
            e.preventDefault();
            $.ajax({
                url: 'update_teacher_salary_by_accountant',
                method: 'post',
                data: new FormData(this),
                processData:false,
                dataType: 'JSON',
                contentType:false,
                cache:false,
                beforeSend:function()
                {
                    $("#updateMessage").html('');
                    $("#updateMessage").removeClass();
                },
                success:function(data)
                {
                    if($data.response == 0)
                    {
                        $.each(data.errors, function(i,v){
                            $("#updateMessage").append('*' + ' ' + v + '<br>');
                        });
                        $("#updateMessage").addClass(data.class);
                    }
                    else
                    {
                        $("#updateMessage").append(data.message);
                        $("#updateMessage").addClass(data.class);
                    }
                }
            });
        });
    });  
</script>
@endsection