<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accountants', function (Blueprint $table) {
            $table->id();
            $table->string('accountant_profile_pic');
            $table->string('user_name')->unique();
            $table->string('password');
            $table->string('name');
            $table->string('father_name');
            $table->string('cnic')->unique();
            $table->string('phone_number')->unique();
            $table->string('email')->nullable()->unique();
            $table->string('qualification');
            $table->string('gender');
            $table->string('dob');
            $table->string('address');
            $table->boolean('is_active')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accountants');
    }
}
