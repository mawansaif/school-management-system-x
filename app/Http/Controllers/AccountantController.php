<?php

namespace App\Http\Controllers;

use App\Models\Accountant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AccountantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.accountant.main'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'accountant_profile' => 'image|mimes:jpeg,bmp,png',
            'user_name'          => 'required|unique:accountants|alpha_dash',
            'password'           => 'required|min:8',
            'name'               => 'required',
            'father_name'        => 'required',
            'cnic_number'        => 'required|numeric',
            'phone_number'       => 'required|unique:accountants',
            'email'              => 'nullable|email|unique:accountants',
            'qualification'      => 'required',
            'gender'             => 'required',
            'dob'                => 'required|before:12 years ago',
            'address'            => 'required'
        ]);
        $validator->after(function ($validator) {
	
            if (User::where('user_name',request('user_name'))
                      ->first()) {
                //add custom error to the Validator
                $validator->errors()->add('user_name', 'User name already exists.');
            }
            elseif(User::where('nic_no', request('cnic_number'))
                        ->first())
            {
                $validator->errors()->add('email', 'User cnic already exists.');
            }
            elseif(User::where('email', request('email'))
                        ->first())
            {
                $validator->errors()->add('email', 'User email already exists.');
            }
        });
        if($validator->fails())
        {
            $data =[
                'response' => 0,
                'errors'   => $validator->errors()->all(),
                'class'    => 'alert alert-danger',
            ];
            return response()->json($data);
        }
        else
        {
            if($request->file('accountant_profile'))
            {
                $data = new Accountant;
                $profile = $request->file('accountant_profile');
                $new_name_of_profile = time().$profile->getClientOriginalName(); 
                $destinationPath = 'accountant_profile_pics';
                $check = $profile->move($destinationPath,$new_name_of_profile);
                if($check)
                {
                    $data->accountant_profile_pic = 'accountant_profile_pics/'.$new_name_of_profile;
                    $data->user_name              = $request->user_name;
                    $data->password               = $request->password;
                    $data->name                   = $request->name;
                    $data->father_name            = $request->father_name;
                    $data->cnic                   = $request->cnic_number;
                    $data->phone_number           = $request->phone_number;
                    $data->email                  = $request->email;
                    $data->qualification          = $request->qualification;
                    $data->gender                 = $request->gender;
                    $data->dob                    = $request->dob;
                    $data->address                = $request->address;
                    $check = $data->save();
                    if($check)
                    {
                        $check = User::create([
                            'user_role' => 'accountant',
                            'name' => $request->name,
                            'user_name' => $request->user_name,
                            'email' => $request->email,
                            'nic_no' => $request->cnic_number,
                            'password' => Hash::make($request->password),
                            'user_profile_pic' => 'accountant_profile_pics/'.$new_name_of_profile,
                        ]);
                        if($check)
                        {
                            $data =[
                                'response'  => 1,
                                'message'   => 'Accountant save successfully.',
                                'class'     => 'alert alert-success',
                            ];
                            return response()->json($data);
                        }
                    }
                }
            }
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Accountant  $accountant
     * @return \Illuminate\Http\Response
     */
    public function show(Accountant $accountant)
    {
        //
        $data = Accountant::orderby('id', 'DESC')
                        ->get();
        return view('admin.accountant.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Accountant  $accountant
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Accountant $accountant)
    {
        //
        $accountant = Accountant::where('id', $request->employ)
                                ->first();
        $user       = User::where('user_name', $accountant->user_name)
                            ->first();
        $data = [
            'accountant'    => $accountant,
            'user'          => $user,
        ];
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Accountant  $accountant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Accountant $accountant)
    {
        //
        $validator = Validator::make($request->all(), [
            'accountant_profile' => 'image|mimes:jpeg,bmp,png',
            'user_name'          => 'required||alpha_dash',
            'password'           => 'required|min:8',
            'name'               => 'required',
            'father_name'        => 'required',
            'cnic_number'        => 'required|numeric',
            'phone_number'       => 'required|',
            'email'              => 'nullable|email',
            'qualification'      => 'required',
            'gender'             => 'required',
            'dob'                => 'required|before:12 years ago',
            'address'            => 'required'
        ]);
        $validator->after(function ($validator) {
    
            if (Accountant::where('id', '!=' ,request('accountant_id'))
                            ->where('user_name', request('user_name'))
                      ->first())
            {
                //add custom error to the Validator
                $validator->errors()->add('user_name', 'User name already exists.');
            }
            elseif(Accountant::where('id', '!=' ,request('accountant_id'))
                                ->where('cnic', request('cnic_number'))
                                ->first())
            {
                //add custom error to the Validator
                $validator->errors()->add('cnic', 'CNIC number already exists.');
            }
            elseif(Accountant::where('id', '!=' ,request('accountant_id'))
                                ->where('phone_number', request('phone_number'))
                                ->first())
            {
                //add custom error to the Validator
                $validator->errors()->add('phone_number', 'phone number already exists.');
            }
            elseif(Accountant::where('id', '!=' ,request('accountant_id'))
                                ->where('email', request('email'))
                                ->first())
            {
                //add custom error to the Validator
                $validator->errors()->add('email', 'Email already exists.');
            }
            elseif (User::where('id', '!=',request('user_id'))
                        ->where('user_name',request('user_name'))
                        ->first()) {
                //add custom error to the Validator
                $validator->errors()->add('user_name', 'User name already exists.');
            }
            elseif(User::where('id', '!=',request('user_id'))
                        ->where('nic_no', request('cnic_number'))
                        ->first())
            {
                $validator->errors()->add('cnic', 'User cnic already exists.');
            }
            elseif(User::where('id', '!=',request('user_id'))
                        ->where('email', request('email'))
                        ->first())
            {
                $validator->errors()->add('email', 'User email already exists.');
            }
        });
        if($validator->fails())
        {
            $data =[
                'response' => 0,
                'errors'   => $validator->errors()->all(),
                'class'    => 'alert alert-danger',
            ];
            return response()->json($data);
        }
        else
        {
            if($request->file('accountant_profile'))
            {
                $data = new Accountant;
                $profile = $request->file('accountant_profile');
                $new_name_of_profile = time().$profile->getClientOriginalName(); 
                $destinationPath = 'accountant_profile_pics';
                $check = $profile->move($destinationPath,$new_name_of_profile);
                if($check)
                {
                    $check = Accountant::where('id', $request->accountant_id)
                    ->update([
                        'accountant_profile_pic' => 'accountant_profile_pics/'.$new_name_of_profile,
                        'user_name'              => $request->user_name,
                        'password'               => $request->password,
                        'name'                   => $request->name,
                        'father_name'            => $request->father_name,
                        'cnic'                  =>  $request->cnic_number,
                        'phone_number'          => $request->phone_number,
                        'email'                 => $request->email,
                        'qualification'         => $request->qualification,
                        'gender'                =>  $request->gender,
                        'dob'                   => $request->dob,
                        'address'               => $request->address,
                        'is_active'             => $request->status
                    ]); 
                    if($check)
                    {
                        $check = User::where('id', $request->user_id)
                        ->update([
                            'name' => $request->name,
                            'user_name' => $request->user_name,
                            'email' => $request->email,
                            'nic_no' => $request->cnic_number,
                            'password' => Hash::make($request->password),
                            'is_active'             => $request->status,
                            'user_profile_pic' => 'accountant_profile_pics/'.$new_name_of_profile,
                        ]);
                        if($check)
                        {
                            $data =[
                                'response'  => 1,
                                'message'   => 'Accountant info update successfully.',
                                'class'     => 'alert alert-success',
                            ];
                            return response()->json($data);
                        }
                    }
                }
            }
            else
            {
                $check = Accountant::where('id', $request->accountant_id)
                ->update([
                    'user_name'              => $request->user_name,
                    'password'               => $request->password,
                    'name'                   => $request->name,
                    'father_name'            => $request->father_name,
                    'cnic'                  =>  $request->cnic_number,
                    'phone_number'          => $request->phone_number,
                    'email'                 => $request->email,
                    'qualification'         => $request->qualification,
                    'gender'                =>  $request->gender,
                    'dob'                   => $request->dob,
                    'address'               => $request->address,
                    'is_active'             => $request->status
                ]); 
                if($check)
                {
                    $check = User::where('id', $request->user_id)
                    ->update([
                        'name' => $request->name,
                        'user_name' => $request->user_name,
                        'email' => $request->email,
                        'nic_no' => $request->cnic_number,
                        'password' => Hash::make($request->password),
                        'is_active'             => $request->status,
                    ]);
                    if($check)
                    {
                        $data =[
                            'response'  => 1,
                            'message'   => 'Accountant info update successfully.',
                            'class'     => 'alert alert-success',
                        ];
                        return response()->json($data);
                    }
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Accountant  $accountant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Accountant $accountant)
    {
        //
        $data = Accountant::where('id', $request->accountant_id)
                            ->first();
        $check = User::where('user_name', $data->user_name)
                    ->delete();
        if($check)
        {
            $check = Accountant::where('id', $request->accountant_id)
                            ->delete();
            if($check)
            {
                $request->session()->flash('message', 'Accountant delete succuessfully.');
                return redirect()->back();
            }
        }
        
    }

    public function card(Request $request)
    {
        $data = Accountant::where('id', $request->employ)
                            ->first();
        return view('admin.accountant.card',compact('data'));
    }
}
