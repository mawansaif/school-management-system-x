<?php

namespace App\Http\Controllers\Accountant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\Models\Accountant;
use App\Models\StudentFee;
use App\Models\StudentFeeDetail;
use App\Models\StudentAttendance;
use Illuminate\Support\Facades\Validator;
use App\Models\Teacher;
use App\Models\TeacherSalary;
use App\Models\SalaryDetail;
use Carbon;

class AccountantController extends Controller
{
    //
    public function student_fees(Request $request)
    {
        $data = [
            'students' => StudentFee::with('students')->get(),
        ];
        return view('accountant.student_feeses.main',compact('data'));
    }

    Public function fee_vouchers(Request $request)
    {
        $student = StudentFee::with('students')
                                ->where('id', $request->student)
                                ->first();

        $fee = StudentFeeDetail::with('student_fees')
                                ->where('student_fee',$student->id)
                                ->get();
        $data = [
            'student'  => $student,
            'fee'      => $fee
         ];

        return view('accountant.student_feeses.detail.main',compact('data'));
    }

    public function print_fee_voucher(Request $request)
    {
        $fee   =   StudentFeeDetail::with('student_fees')
                                        ->where('id', $request->voucher)
                                        ->first();
        $attendance_fine = StudentAttendance::where('attendance', 'absent')
                                             ->whereYear('attendance_date',  Carbon\Carbon::parse($fee->fee_of_month)->format('Y'))
                                             ->whereMonth('attendance_date', Carbon\Carbon::parse($fee->fee_of_month)->format('m'))
                                             ->count();
        $data = [
            'fee' => $fee,
            'attendance_fine'   => $attendance_fine
        ];
        return view('accountant.student_feeses.detail.feevoucher', compact('data'));
    }
    
    public function edit_student_fee(Request $request)
    {
        $fee_detail = StudentFeeDetail::with('student_fees')
                                        ->where('id', $request->student_fee_id)
                                        ->first();
        return response()->json($fee_detail);
    }
    public function update_student_fee(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'student_fee_detail_id' => 'required',
        ]);
        if($validator->fails())
        {
            $data = [
                'response'  => 0,
                'errors'    => $validator->errors()->all(),
                'class'     => 'alert alert-danger'
            ];

            return response()->json($data);
        }
        else
        { 
            if($request->is_paid == 1 and ($request->paid_date == ''))
            {
                $data = [
                    'response'  => 0,
                    'errors'    => ['Please provide date at which fee pay because you select yes in pay option.'],
                    'class'     => 'alert alert-danger'
                ];
    
                return response()->json($data);
            }
            elseif($request->is_paid == 0)
            {
                
                $check = StudentFeeDetail::where('id', $request->student_fee_detail_id)
                                        ->update([
                                            'fee_of_month' => $request->fee_of_month,
                                            
                                        ]);
                if($check)
                {
                    $data = [
                        'response'  => 1,
                        'message'    => ['Student fee detail is updated successfully.'],
                        'class'     => 'alert alert-success'
                    ];
        
                    return response()->json($data);
                }
            }
            elseif($request->is_paid == 1 and ($request->paid_date != ''))
            {
                $check = StudentFeeDetail::where('id', $request->student_fee_detail_id)
                                            ->update([
                                                'paid_date' => $request->paid_date,
                                                'is_paid'   => $request->is_paid
                                            ]);
                if($check)
                {
                    $fee = StudentFeeDetail::where('id',$request->student_fee_detail_id)->first();
                    $paidFee = $fee->fee_amount;
                    $mainFee = StudentFee::where('id',$fee->student_fee)
                                            ->first(); 
                    $GrandPaidFee = $mainFee->paid_fee + $paidFee;
                    $remaining_fee = $mainFee->student_fee - $GrandPaidFee;
                    $check = StudentFee::where('id', $fee->student_fee)
                                        ->update([
                                            'paid_fee' => $GrandPaidFee,
                                            'remaining_fee' => $remaining_fee,
                                        ]);
                    if($check)
                    {
                        $data = [
                            'response'  => 1,
                            'message'    => ['Student fee detail is updated successfully.'],
                            'class'     => 'alert alert-success'
                        ];
            
                        return response()->json($data);
                    }

                }
            }
        }
    }

    public function teacher_salaries(Request $request)
    {
        $salary = TeacherSalary::orderBy('id', 'DESC')
                                ->whereHas(
                                    'teachers', function ($query) {
                                        $query->where('is_active', 1);
                                    }
                                )
                                ->with("teachers")
                                ->get();
        $teacher = Teacher::orderBy('id','DESC')
                            ->where('is_active', 1)
                            ->get();
        $data = [
            'teacher' => $teacher,
            'salary' => $salary,
        ];
        return view('accountant.teacher_salaries.main',compact('data'));
    }

    public function teacher_salary_detail(Request $request)
    {
        $teacher    = Teacher::where('id', $request->teacher)
                            ->first();
        $salary = TeacherSalary::where('teacher', $request->teacher)
                                ->first();
        $salarydetail = SalaryDetail::with('teachers','teacherSalary')
                              ->where('teacher_id', $request->teacher)
                              ->get();
        $data = [
            'teacher' => $teacher,
            'salary'  => $salary,
            'salarydetail'  => $salarydetail
        ];

        
        return view('accountant.teacher_salaries.detail.main',compact('data'));
    }


    public function add_teacher_salary(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'teacher'   => 'required',
            'salary_amount'    => 'required|numeric',
            'advance_salary' => 'required|numeric',
            'salary_of_month' => 'required|date',
            'remaining_salary' => 'required',
        ]);
        $validator->after(function ($validator) {

            if (SalaryDetail::where('teacher_id', request('teacher'))
                              ->whereDate('salary_of_month', request('salary_of_month'))
                                ->exists()) {
                $validator->errors()->add('salary_of_month', 'Salary of this month is already exists.');
            }
        
        });
        if($validator->fails())
        {
           

            $data = [
                'response' => 0,
                'errors'   => $validator->errors()->all(),
                'class'    => 'alert alert-danger'
            ];
            return response()->json($data);
        }
        else
        {
            
            $salary = TeacherSalary::where('id', $request->salary_id)
                                    ->first();

            $data = new SalaryDetail;
            $data->salary_id  = $request->salary_id;
            $data->teacher_id = $request->teacher;
            $data->advance_salary  = $request->advance_salary;
            $data->salary_of_month  = $request->salary_of_month;
            $data->remaining_salary  = $salary->salary - $request->advance_salary;
            if($data->save())
            {
                $data = [
                    'response' => 1,
                    'message'  => 'Teacher Salary add successfully.',
                    'class'    => 'alert alert-success',
                ];
                return response()->json($data);
            }
            

        }
    }

    public function edit_teacher_salary(Request $request)
    {
        $data = SalaryDetail::where('id', $request->salary_id)
                            ->first();
        return response()->json($data);
    }

    public function update_teacher_salary(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'edit_salary_id' => 'required',
            'teacher'        => 'required',
            'salary'         => 'required|numeric',
            'advance_salary' => 'required|numeric',
            'salary_of_month'=> 'required|date',
            'remaining_salary' => 'required|numeric',
        ]);
        $validator->after(function ($validator) {

            if (SalaryDetail::where('id', '!=' ,request('edit_salary_id'))
                                ->where('teacher_id', request('teacher'))
                                ->whereDate('salary_of_month', request('salary_of_month'))
                                ->exists()) {
                $validator->errors()->add('salary_of_month', 'Salary of this month is already exists.');
            }
        
        });
        if($validator->fails())
        {
            $data = [
                'response'  => 0,
                'errors'    => $validator->errors()->all(),
                'class'     => 'alert alert-danger',
            ];
            return response()->json($data);
        }
    }
}
